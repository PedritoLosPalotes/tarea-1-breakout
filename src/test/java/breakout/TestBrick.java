package breakout;

import static org.junit.Assert.assertEquals;
import org.junit.Test;

import com.cc3002.breakout.logic.IBrick.IBrick;
import com.cc3002.breakout.logic.IBrick.isSoftBrick;
import com.cc3002.breakout.logic.IBrick.isStoneBrick;

public class TestBrick {
  
  @Test 
  public void testSoft() {
    final IBrick sbrick = new isSoftBrick();
    assertEquals("",10,sbrick.points());
    assertEquals("",1,sbrick.remainingHits()); 
  }

  
  @Test
  public void testStone() {
    final IBrick hbrick = new isStoneBrick();
    assertEquals("",50,hbrick.points());
    assertEquals("",3,hbrick.remainingHits());
  }  
  
  @Test
  public void testhits() {
    final IBrick brick = new isStoneBrick();
    assertEquals("debe dar 2",2,brick.hits());
    assertEquals("debe dar 1",1,brick.hits());
    assertEquals("debe dar 0",0,brick.hits());
    assertEquals("debe dar -1",-1,brick.hits());  
  }
  
  @Test 
  public void testHitSoft() {
    
    final IBrick brick = new isSoftBrick();
    assertEquals("debe dar 0",0,brick.hits());
  }
 
}
