/**

 * 
 */

package com.cc3002.breakout.logic.IBrick;

/**
 * @author selva
 *
 */
public interface IBrick {

  int hits();

  int points();
  
  int remainingHits();

}
