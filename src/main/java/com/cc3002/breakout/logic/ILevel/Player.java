package com.cc3002.breakout.logic.ILevel;

public class Player {

  public int vidas = 3;
  public long score;
  
  public int getNumberOfHearts() {
    
    return vidas;
  }
  
  public int lossOfHeart() {
    vidas = vidas - 1 ;
    return vidas;
  }
  
  public long earnedScore() {
    return score;
  }
}
